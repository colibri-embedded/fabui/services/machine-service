set(SOURCES
	"${PROJECT_SOURCE_DIR}/src/main.cpp"
	"${PROJECT_SOURCE_DIR}/src/machine_service.cpp"
	"${PROJECT_SOURCE_DIR}/src/machine_service_wamp_wrapper.cpp"
	)

add_executable(machine-service ${SOURCES})
set_property(TARGET machine-service PROPERTY CXX_STANDARD 14)
set_property(TARGET machine-service PROPERTY CXX_STANDARD_REQUIRED ON)

target_include_directories(machine-service PUBLIC ${3RDPARTY_SOURCE_DIR})
target_include_directories(machine-service PUBLIC ${CURL_INCLUDE_DIR})

add_definitions(${WAMPCC_CFLAGS} ${WAMPCC_CFLAGS_OTHER} )

target_link_libraries (machine-service ${LIBFABUI_LIBRARIES} ${YAML_CPP_LIBRARIES})

##
## Install targets
##
install (TARGETS machine-service DESTINATION "${INSTALL_BIN_DIR}")
