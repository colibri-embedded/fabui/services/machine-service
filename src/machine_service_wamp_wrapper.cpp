/**
 * Copyright (C) 2018 Colibri-Embedded
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @file: machine_service_wamp_wrapper.cpp
 * @brief MachineService wamp wrapper implementation
 *
 * @author Daniel Kesler <kesler.daniel@gmail.com>
 */
#include "config.h"
#include "machine_service_wamp_wrapper.hpp"

using namespace fabui;
using namespace wampcc;
using namespace std::placeholders;

MachineServiceWrapper::MachineServiceWrapper(const std::string& env_file, MachineService& service)
	: BackendService(PACKAGE_STRING, env_file)
	, m_service(service)
{
	m_service.setWampSession(this);
}

void MachineServiceWrapper::onConnect()
{
	join( realm(), {"token"}, "machine-service");
}

void MachineServiceWrapper::onDisconnect()
{
	m_service.stop();
}

void MachineServiceWrapper::onJoin()
{
	std::cout << "Joined\n";
	/*auto info = call("backend.get_url");
	info.wait();
	auto result = info.get();
	if(!result.was_error) {
		http.setBaseUrl( result.args.args_list[0].as_string() );
	}*/
	provide("machine.service.terminate", std::bind(&MachineServiceWrapper::terminate_wrapper, this, _1));
	
	provide("machine.info", std::bind(&MachineServiceWrapper::info_wrapper, this, _1));

	// provide("machine.head.info", std::bind(&MachineServiceWrapper::headInfo_wrapper, this, _1));
	// provide("machine.head.list", std::bind(&MachineServiceWrapper::headList_wrapper, this, _1));
	// provide("machine.head.set", std::bind(&MachineServiceWrapper::headSet_wrapper, this, _1));

	// provide("machine.bed.info", std::bind(&MachineServiceWrapper::bedInfo_wrapper, this, _1));	
	// provide("machine.bed.list", std::bind(&MachineServiceWrapper::bedList_wrapper, this, _1));
	// provide("machine.bed.set", std::bind(&MachineServiceWrapper::bedSet_wrapper, this, _1));
	
	// provide("machine.feeder.info", std::bind(&MachineServiceWrapper::feederInfo_wrapper, this, _1));	
	// provide("machine.feeder.list", std::bind(&MachineServiceWrapper::feederList_wrapper, this, _1));	
	// provide("machine.feeder.set", std::bind(&MachineServiceWrapper::feederSet_wrapper, this, _1));	

	provide("controller.reset", std::bind(&MachineServiceWrapper::controllerReset_wrapper, this, _1));

	provide("gcode.macro.run", 	std::bind(&MachineServiceWrapper::macroRun_wrapper, this, _1));

	std::cout << PACKAGE_NAME" started\n" << std::flush;
}

void MachineServiceWrapper::info_wrapper(invocation_info info) {
	try {
		auto result = m_service.info();
		yield(info.request_id, {}, result);
	} catch(const std::exception &e) {
		/* Return error */
		json_array errors = { e.what() };
		error(info.request_id, "wamp.error.runtime_error", errors);
	}
}

void MachineServiceWrapper::terminate_wrapper(wampcc::invocation_info info) {
	yield(info.request_id);
	disconnect();
}

void MachineServiceWrapper::controllerReset_wrapper(wampcc::invocation_info info) {
	try {
		auto result = m_service.controllerReset();
		yield(info.request_id, {result});
	} catch(const std::exception &e) {
		json_array errors = { e.what() };
		error(info.request_id, "wamp.error.runtime_error", errors);
	}
}

void MachineServiceWrapper::macroRun_wrapper(wampcc::invocation_info info) {
	try {

		std::string macro = getArgumentOrFail(0, info.args.args_list).as_string();
		std::cout << "MACRO: " << macro << std::endl;
		auto result = m_service.macroRun(macro, info.args.args_dict);
		std::cout << "MACRO(finished): " << macro << std::endl;
		yield(info.request_id, {result});
	} catch(const std::exception &e) {
		json_array errors = { e.what() };
		error(info.request_id, "wamp.error.runtime_error", errors);
	}

}