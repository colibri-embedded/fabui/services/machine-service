#include "config.h"
#include "machine_service.hpp"

#include <cassert>
#include <chrono>
#include <iostream>
#include <cstdlib>

#include <yaml-cpp/yaml.h>
#include <fabui/utils/string.hpp>

using namespace fabui;
using namespace wampcc;
using namespace std::placeholders;

MachineService::MachineService(const std::string& machine_file) 
	: m_reset_duration_ms(1000)
{
	// std::cout << "loading " << machine_file << std::endl;
	loadSpecs(machine_file);
}

MachineService::~MachineService() 
{

}

void MachineService::setWampSession(IWampSession *session)
{
	m_session = session;
}

IWampSession* MachineService::getWampSession()
{
	return m_session;
}

void MachineService::stop() 
{
	// NOTE: placeholder
}

bool MachineService::parseNode(const YAML::Node& node, const std::string& root)
{
	// std::cout << "path: " << root << std::endl;

	for(auto item: node) {
		// std::cout << item.first << std::endl;
		std::string key = item.first.as<std::string>();
		std::string new_root = root;
		if(!root.empty())
			new_root += ".";
		new_root += key;

		if(item.second.IsMap() ) {
			// std::cout << "key: " << item.first << " - " << new_root << std::endl;
			parseNode(item.second, new_root);
		} else {
			// std::cout << "value: " << item.second << std::endl;
			// std::cout << "key: " << new_root << std::endl;

			if(item.second.IsScalar()) {
				auto value = item.second.as<std::string>();
				//std::cout << new_root << " = " << value << std::endl;
				m_spec[new_root] = value;
			} else {
				std::cout << "value can only be a scalar\n";
				return false;
			}
		}
	}

	return true;
}

bool MachineService::haveSetting(const std::string& key) 
{
	for(auto& item: m_spec) {
		if(item.first == key)
			return true;
	}
	return false;
}

bool MachineService::loadSpecs(const std::string& filename) 
{
	YAML::Node m_machine_spec = YAML::LoadFile(filename);

	auto result = parseNode(m_machine_spec);

	// add defaults
	if( not haveSetting("reset.type") ) {
		m_spec["machine.reset.type"] = "serial";
		m_spec["machine.reset.duration"] = "1000";
	}

	m_reset_duration_ms = std::atoi(m_spec["machine.reset.duration"].c_str());

	return result;
}

json_object MachineService::info() 
{
	json_object data;

	for(auto &item: m_spec) {
		data[ item.first ]  = item.second;
	}

	return data;
}

bool MachineService::controllerReset() 
{
	using namespace std::chrono_literals;
	try {
		auto cfut = m_session->call("serial.close", { {true} }, {});
		cfut.wait();
		auto result = cfut.get();
		if(result.was_error) {
			std::cout << "serial.close: FAILED " << result.error_uri << std::endl;
		}

		std::this_thread::sleep_for( std::chrono::milliseconds(m_reset_duration_ms) );

		cfut = m_session->call("serial.open", {}, {});
		cfut.wait();
		result = cfut.get();
		if(result.was_error) {
			std::cout << "serial.open: FAILED "  << result.error_uri << std::endl;
		}
	} catch (const std::exception& e) {
		std::cout << "controllerReset.error: " << e.what() << std::endl;
		return false;
	}

	return true;
}

bool MachineService::macroRun(const std::string& macro_name, const wampcc::json_object& env) 
{
	std::cout << "RUN: " << macro_name << std::endl;

	auto macro_name_full = "macros." + macro_name;

	auto macro = m_spec.find(macro_name_full);
	if( macro == m_spec.end() )
		return true;

	auto lines = utils::str::split( macro->second, '\n' );
	for(auto &line : lines) {
		if(!line.empty()) {
			unsigned timeout = 5000; // 5s

			if(line.compare(0, 4, "M400") == 0) {
				timeout = 30 * 1000; // 30s
			}

			std::cout << "[" <<  line << "]\n";

			try {
				auto cfut = m_session->call("gcode.send", {{}, {
					{"gcode", line},
					{"timeout", timeout}
				}}, {});

				cfut.wait();
				auto result = cfut.get();

				if(result.was_error) {
					std::cout << "Failed to execute " << line << ": " << result.error_uri << std::endl;
					std::cout << result.args.args_list << std::endl;
					return false;
				}				
			} catch(const std::exception &e) {
				throw;
			}

		}
		
	}
	return true;
}
