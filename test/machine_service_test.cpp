#define CATCH_CONFIG_MAIN  // This tells Catch to provide a main() - only do this in one cpp file
#include <catch2/catch.hpp>
#include <machine_service.hpp>

#include <future>
#include <vector>
#include <memory>
#include <map>
#include <fabui/wamp/mock/session.hpp>

using namespace fabui;
using namespace std::placeholders;
using namespace std::literals;

/****** Constants *******/

#define TIC std::chrono::high_resolution_clock::duration tic_time = std::chrono::high_resolution_clock::now().time_since_epoch()
#define TOC std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::high_resolution_clock::now().time_since_epoch()-tic_time).count()

/******* Helpers *******/

// class GCodeServiceSpy: public GCodeService {
// 	public:
// 		GCodeServiceSpy(const std::string& portName = UART_PORT,
// 			unsigned baudRate=UART_BAUDRATE, 
// 			unsigned timeout=200) 
// 			: GCodeService(portName, baudRate, timeout) {

// 			}

// 		bool isSerialOpen() {
// 			return m_serial.isOpen();
// 		}

// 		bool isRunning() {
// 			std::lock_guard<std::mutex> guard(m_running_mut);
// 			return m_running_state == State::RUNNING;
// 		}

// 		bool isSuspended() {
// 			std::lock_guard<std::mutex> guard(m_running_mut);
// 			return m_running_state == State::SUSPENDED;
// 		}
// };


class Delayed {
	public:
		Delayed(unsigned delay, std::function<void()> callback) 
			: m_delay(delay)
			, m_callback(callback)
		{
			
		}

		~Delayed() {
			if(m_thread.joinable())
				m_thread.join();
		}

		void start() {
			m_thread = std::thread(&Delayed::loop, this);
		}

		void loop() {
			std::this_thread::sleep_for( std::chrono::milliseconds(m_delay)  );
			m_callback();
		}

		std::thread m_thread;

		unsigned m_delay;
		std::function<void()> m_callback;
};

/***** TESTS *****/

SCENARIO("New object state transition", "[machine-service][states]") {

	WHEN("New object is created -> destroy") {

		THEN("Object is destroyed successfully") {
			MachineService ms(DATA_DIR"/machine/prusa3d_i3_mk2.yaml");
		}

	}

}


SCENARIO("Constroller reset", "[machine-service][reset]") {

	WHEN("controllerReset is called on serial type") {
		MachineService ms(DATA_DIR"/machine/prusa3d_i3_mk2.yaml");
		WampSessionMock wamp;
		ms.setWampSession(&wamp);

		auto info = ms.info();

		REQUIRE( info["machine.reset.type"] == "serial" );
		REQUIRE( info["machine.reset.duration"] == "1000" );

		THEN("Serial port is closed and opened") {

			auto fut1 = wamp.getCallMatchFuture("serial.close", 1);
			auto fut2 = wamp.getCallMatchFuture("serial.open", 1);

			REQUIRE( ms.controllerReset() );

			auto s1 = fut1.wait_for(3s);
			REQUIRE( s1 == std::future_status::ready );

			auto s2 = fut2.wait_for(3s);
			REQUIRE( s2 == std::future_status::ready );
		}
	}

	WHEN("controllerReset is called on gpio type") {
		// TODO
	}

}

SCENARIO("Macro execution", "[machine-service][macro]") {

	WHEN("macroRun is called") {
		MachineService ms(DATA_DIR"/machine/prusa3d_i3_mk2.yaml");
		WampSessionMock wamp;
		ms.setWampSession(&wamp);

		THEN("Propper gcode commands are executed") {
			auto fut1 = wamp.getCallMatchFuture("gcode.send", 2);

			REQUIRE( ms.macroRun("print.start") );

			auto s1 = fut1.wait_for(3s);
			REQUIRE( s1 == std::future_status::ready );

			REQUIRE( wamp.m_called[0].args.args_dict["gcode"].as_string() == "G90" );
			REQUIRE( wamp.m_called[1].args.args_dict["gcode"].as_string() == "M75" );
		}

	}

}