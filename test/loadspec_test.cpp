#define CATCH_CONFIG_MAIN  // This tells Catch to provide a main() - only do this in one cpp file
#include <catch2/catch.hpp>
#include <machine_service.hpp>

#include <future>
#include <vector>
#include <memory>
#include <map>

using namespace fabui;
using namespace std::placeholders;
using namespace std::literals;

SCENARIO("Load specs from a yaml file", "[machine-service][loacspec]") {

	WHEN("New object is created ") {

		THEN("Specs are loaded") {
			MachineService ms(DATA_DIR"/machine/prusa3d_i3_mk2.yaml");

			auto info = ms.info();

			REQUIRE( info["company.name"] == "Prusa Research" );
			REQUIRE( info["company.slug"] == "prusa3d" );

			REQUIRE( info["machine.name"] == "Original Prusa i3 MK2" );
			REQUIRE( info["machine.slug"] == "i3_mk2" );
			REQUIRE( info["machine.version"] == "2" );

			REQUIRE( info["machine.command.format"] == "gcode" );
			REQUIRE( info["machine.gcode.flavour"] == "marlin-1.1.x" );

			REQUIRE( info["macros.print.start"] == "G90\nM75\n" );
			REQUIRE( info["macros.print.pause"] == "M76\n" );
			REQUIRE( info["macros.print.resume"] == "M75\n" );
			REQUIRE( info["macros.print.finish"] == "M77\n" );
			REQUIRE( info["macros.print.abort"] == "M117 Aborting...\nM400\nG91\nG0Z+10F1500\nM400\nM140 S0\nM104 S0\nM107\nM117 Aborted\nM77" );
		}

	}

}
