/**
 * Copyright (C) 2018 Colibri-Embedded
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @file: machine_service_wamp_wrapper.hpp
 * @brief MachineService wamp wrapper definition
 *
 * @author Daniel Kesler <kesler.daniel@gmail.com>
 */
#ifndef MACHINE_SERVICE_WAMP_WRAPPER_HPP
#define MACHINE_SERVICE_WAMP_WRAPPER_HPP

#include "machine_service.hpp"
#include <fabui/wamp/backend_service.hpp>

namespace fabui {

class MachineServiceWrapper: public BackendService {
	public:
		MachineServiceWrapper(const std::string& env_file, MachineService& service);

	protected:
		MachineService& m_service;

		/* backend-service */
		void onConnect();
		void onDisconnect();
		void onJoin();

		void terminate_wrapper(wampcc::invocation_info info);
		void info_wrapper(wampcc::invocation_info info);

		void controllerReset_wrapper(wampcc::invocation_info info);
		void macroRun_wrapper(wampcc::invocation_info info);

};

} // namespace fabui

#endif /* MACHINE_SERVICE_WAMP_WRAPPER_HPP */